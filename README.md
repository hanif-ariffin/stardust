# Stardust

A simple 'game', there is not much to it, you move around and there will be trails of you behind. Although it does not exactly have any 'momentum' or 'force'
physics implemented but it still ultimately have the same effect i.e. if your 'space' is moving left, it takes time to change course and move to the right.

This program is made possible by utilizing the SFML library
